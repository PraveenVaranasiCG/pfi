
Tools to cover:
    Cloud Platforms: AWS, Azure
    CI/CD Tools:    Jenkins, Bamboo
    Containerization:   Docker
    Container orchestration: Kubernetes
    Configuration management: Terraform, Ansible
    Programming: Python, Shell
    Monitoring: Prometheus, Grafana
    Version control: git

Tools list:
---------------------
    * Python
    * Shell
    * Linux concepts
    * Networking
    * AWS
    * Azure
    * Git
    * Jenkins
    * Bamboo
    * Docker
    * Kubernetes
    * Terraform
    * Ansible
    * Prometheus
    * Grafana
---------------------